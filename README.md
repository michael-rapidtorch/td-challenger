# myapp-cookbook

An application cookbook to deploy my application.

## Supported Platforms

* Debian GNU/Linux
* Ubuntu Linux

## Description

This application consists from 2 types of processes; master and slaves.

* Both master and slave will act as a HTTP server
* Master and slave will be deployed on different servers.
* Slaves can be multiple even on single server.
* The application need to be configured to use different TCP port, data and log directory per process on a single server.

## Challenges

1. Please modify cookbook to deploy 3 application processes per slave server
2. Please point out some problems in the cookbook implementations
3. Please apply some fixes for the problems you pointed out

## License and Authors

Author:: Treasure Data, Inc. (<yuu@treasure-data.com>)
