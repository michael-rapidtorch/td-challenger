default["server.role.type"] =  'master'

default["data.dir"] = 'data' 
default["logs.dir"] = 'logs' 

default['master']['port'] = 8080
default['slave']['port'] = 8081

default['process.config'] = "test"


