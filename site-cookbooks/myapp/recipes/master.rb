#
# Cookbook Name:: myapp
# Recipe:: master
#
# Copyright (C) 2015 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#


app = data_bag_item('myapp', node.chef_environment)
node.run_state[:current_app] = app
node.default["server.role.type"] = 'master'

current_process = node["current"]["process"]
current_server = node["current"]["server"]
current_cluster = node["current"]["cluster"]
cluster_name = node["myapp"]["cluster_name"]

servicename = "myapp-#{node["server.role.type"]}-#{node["current"]["process"]}"

node.default["service.name"] = servicename
node.default["cluster.name"] = current_cluster
node.default["cluster.name"] = cluster_name 
node.default["process.config"] = app['clusters'][current_cluster]['nodes'][current_server]['processes'][current_process] 
node.default["data.dir"] = node["current"]["data.dir"]
node.default["logs.dir"] = node["current"]["logs.dir"]
node.default["database"]["password"] = node["current"]["database_pw"]

