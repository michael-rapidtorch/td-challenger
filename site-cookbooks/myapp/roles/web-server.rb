name 'web_server'
description 'Monitoring server'

run_list(
  'recipe[role-web-server]'
)

default_attributes(
  :paths => {
    :deploy_to    => '/srv/a',
    :log_path     => '/srv/b',
    :shared_path  => '/srv/c',
    :data_path    => '/srv/d'
  }
)



