#
# Cookbook Name:: role-web-server
# Recipe:: default
#
# Copyright (C) 2015 YOUR_NAME
include_recipe "apt"
include_recipe "git"
include_recipe "runit"
package "ruby"

app = node.run_state[:current_app]
app_env = app['app_env']
cluster_name = node['cluster.name']
cluster_overwrites = app['clusters'][cluster_name] || {}

data_path = node['data.dir']
log_path = node['logs.dir']

overwrites = node['process.config'].merge(cluster_overwrites)
server_type = node['server.role.type']
service_name = node["service.name"]

deploy_to = "/srv/#{service_name}"

directory deploy_to do
  owner 'nobody'
  group 'nogroup'
  mode '0755'
  recursive true
end

directory log_path do
  owner 'nobody'
  group 'nogroup'
  mode '0755'
  recursive true
end

directory data_path do
  owner 'nobody'
  group 'nogroup'
  mode '0755'
  recursive true
end

directory "#{deploy_to}/shared" do
  owner 'nobody'
  group 'nogroup'
  mode '0755'
  recursive true
end


server_config = lambda do |key,default_value|
  # take overwrites[key:server_type] first.
  # if it's not set, take overwrites[key]
  v = overwrites["#{key}:#{server_type}"]
  v = overwrites[key] if v.nil?
  v = default_value if v.nil?
  v
end

if server_type == "master"
  server_port = server_config['master_port', 8080]
  enable_master_scheduling = false
else
  server_port = server_config['slave_port', 8081]
  enable_master_scheduling = true
end

current_server_home = "#{deploy_to}/current"

notifies_restart_service = lambda do |resource|
    resource.notifies :restart, "runit_service[#{service_name}]"
end

overwrite_config = lambda do |server_home, server_type|
  {
    'config.properties' => server_config['config', {}].merge({
      'http.port' => server_port,
      'master' => (server_type == "master"),
      'data.dir' => data_path,
      'logs.dir' => log_path,
    }),
  }.each do |fname,params|

    file "#{server_home}/config/#{fname}" do
      owner 'nobody'
      group 'nogroup'
      mode "644"
      action :create
      content TDHelper.overwrite_properties("#{server_home}/config.orig/#{fname}", params)
      notifies_restart_service.call(self)
    end
  end

  {
    'database.yml' => server_config['database', {}],
  }.each do |fname,params|

    file "#{server_home}/config/#{fname}" do
      owner 'nobody'
      group 'nogroup'
      mode "644"
      action :create
      content TDHelper.overwrite_yaml("#{server_home}/config.orig/#{fname}", "#{server_home}/config/#{fname}", params, app_env)
      # restart runit_service
      notifies_restart_service.call(self)
    end
  end
end


Chef::Log.info("server_type")
Chef::Log.info(server_type)
Chef::Log.info("currentproc")
Chef::Log.info(node["current"]["process"])



deploy_revision "myapp-#{server_type}-#{node["current"]["process"]}" do
  revision server_config['branch', app['branch']]
  repository app["repository"]
  user 'nobody'
  group 'nogroup'
  deploy_to deploy_to
  action :deploy
  shallow_clone true
  keep_releases 3
  rollback_on_error true  # remove release if callbacks failed

  # disable default behavior
  purge_before_symlink([])
  create_dirs_before_symlink([])
  symlink_before_migrate({})
  symlinks({})

  before_migrate do
    FileUtils.mkdir_p "#{release_path}/config.orig"
    Dir["#{release_path}/config/*"].each {|f|
      FileUtils.cp_r f, "#{release_path}/config.orig/", :dereference_root => true
    }
    overwrite_config.call(release_path, server_type)

    directory ::File.join(release_path, "data") do
      owner 'nobody'
      group 'nogroup'
      mode '0755'
    end

    directory ::File.join(release_path, "logs") do
      owner 'nobody'
      group 'nogroup'
      mode '0755'
    end
  end

  # restart runit_service
  notifies_restart_service.call(self)
end

# refresh config files every time so that changing data bag takes effect
overwrite_config.call(current_server_home, server_type) if File.exists?(current_server_home)
runit_service service_name do
  cookbook "myapp"
  run_template_name "myapp"
  log_template_name "myapp"
  options({
    :app_env => app_env,
    :app_home => current_server_home,
    :data_dir => node.default["data_dir"],
  })
  retries 3
  retry_delay 5
end
#
# All rights reserved - Do Not Redistribute
#
