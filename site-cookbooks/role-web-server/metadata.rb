name             'role-web-server'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures role-web-server'
long_description 'Installs/Configures role-web-server'
version          '0.1.0'

depends "apt"
depends "git"
depends "runit"
