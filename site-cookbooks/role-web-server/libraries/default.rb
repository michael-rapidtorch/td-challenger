module TDHelper
  def self.overwrite_properties(tmpl_path, params)
      attrs = {}
      File.read(tmpl_path).split("\n").map do |line|
        line = line.sub(/#.*$/, '').strip
        j, v = line.split(/\s*=\s*/, 2)
        attrs[j] = v if j
      end
      params.each {|j,v| v.nil? ? attrs.delete(j) : attrs[j] = v }
      attrs.map {|j,v| "#{j}=#{v}\n" }.sort.join
  end 
  def self.overwrite_yaml(tmpl_path, dest_path, params, env)
      envs = {}
      envs = YAML.load_file(tmpl_path) if File.exists?(tmpl_path)
      attrs = (envs[env] ||= {})
      params.each {|k,v|
        v.nil? ? attrs.delete(k) : attrs[k] = v
      }
      data = {env => attrs}
      if data == (YAML.load_file(dest_path) rescue nil)
        File.read(dest_path)
      else
        YAML.dump(data)
      end
  end
  def self.notify_service(service_name)
      resource.notifies :restart, service_name 
  end
end

