# role-web-server-cookbook

Application of a Chef Pattern "role cookbook". As chef roles are unversioned , this cookbook is to allow us to version a web-server role and call it from within our application

## Supported Platforms

Ubuntu 
Debian

## Usage

### role-web-server::default

Include `role-web-server` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[role-web-server::default]"
  ]
}
```

## License and Authors

