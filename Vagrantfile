# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = '2'

Vagrant.require_version '>= 1.5.0'

cluster_config = (JSON.parse(File.read("site-cookbooks/myapp/test_data_bags/myapp/_default.json")))['clusters']
#bodes_config = (JSON.parse(File.read("nodes.json")))['nodes']



Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  config.vm.hostname = 'myapp-berkshelf'
  config.berkshelf.berksfile_path = 'site-cookbooks/myapp/Berksfile'
  # Set the version of chef to install using the vagrant-omnibus plugin
  # NOTE: You will need to install the vagrant-omnibus plugin:
  #
  #   $ vagrant plugin install vagrant-omnibus
  #
  if Vagrant.has_plugin?("vagrant-omnibus")
    config.omnibus.chef_version = 'latest'
  end

  # Every Vagrant virtual environment requires a box to build off of.
  # If this value is a shorthand to a box in Vagrant Cloud then
  # config.vm.box_url doesn't need to be specified.
  config.vm.box = 'chef/ubuntu-14.04'


  # Assign this VM to a host-only network IP, allowing you to access it
  # via the IP. Host-only networks can talk to the host machine as well as
  # any other machines on the same network, but cannot be accessed (through this
  # network interface) by any external networks.
  config.vm.network :private_network, type: 'dhcp'

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider :virtualbox do |vb|
  #   # Don't boot with headless mode
  #   vb.gui = true
  #
  #   # Use VBoxManage to customize the VM. For example to change memory:
  #   vb.customize ["modifyvm", :id, "--memory", "1024"]
  # end
  #
  # View the documentation for the provider you're using for more
  # information on available options.

  # The path to the Berksfile to use with Vagrant Berkshelf
  # config.berkshelf.berksfile_path = "./Berksfile"

  # Enabling the Berkshelf plugin. To enable this globally, add this configuration
  # option to your ~/.vagrant.d/Vagrantfile file
  config.berkshelf.enabled = true

  # An array of symbols representing groups of cookbook described in the Vagrantfile
  # to exclusively install and copy to Vagrant's shelf.
  # config.berkshelf.only = []

  # An array of symbols representing groups of cookbook described in the Vagrantfile
  # to skip installing and copying to Vagrant's shelf.
  # config.berkshelf.except = []


  cluster_config.each do |cluster|
    cluster_name   = cluster[0] # name of node
    nodes_config = (JSON.parse(File.read("site-cookbooks/myapp/test_data_bags/myapp/_default.json")))['clusters'][cluster_name]['nodes']

    nodes_config.each do |node|
      node_name = node[0]
      node_values = node[1]
      node_role = node_values['role']

      run_list = Array.new
      if node_role == 'slave'
         run_list << 'recipe[myapp::slave]' 
      else
         run_list << 'recipe[myapp::master]'
      end
      run_list << 'role[web-server]' 
      config.vm.define node_name do |nodeconfig|  
           processes = node_values['processes']
           processes.each do |process|
              nodeconfig.vm.provision :chef_solo do |chef|
                  chef.data_bags_path = 'site-cookbooks/myapp/test_data_bags'
                  chef.run_list = run_list              
                  chef.roles_path = "site-cookbooks/myapp/roles"
                  chef.json = {
                    "current" => {
                      "cluster" => cluster_name,
                      "server" => node_name,
                      "process" => process[0],
                      "data.dir" => process[1]['data.dir'],
                      "logs.dir" => process[1]['logs.dir'],
                      "database_pw" => process[1]['database']['password']
                    },
                    "myapp" => {
                      "cluster_name" => cluster_name,
                      "role" => node_role
                    },
                  }
              end
           end
        end
    end
  end
end
